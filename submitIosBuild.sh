#!/bin/bash
#####
# This requires running on a mac with Xcode installed and uses the Application Loader tool
# It downloads the build from Jenkins, verifies it, and submits it
# Usage:
# sh submitIosBuild.sh -u <jenkinsUsername> -p <jenkinsPassword> -b <jenkinsIosBuildNumber> -U <appleUsername> -P <applePassword>
#
# Examples:
#
# - Using a Build#
# sh submitIosBuild.sh -u bwayne -p ban@na -b 39 -U batman@amctheatres.com -P @ppl3
#
# - Using 'latest' in place of Build#
# sh submitIosBuild.sh -u bwayne -p ban@na -b latest -U batman@amctheatres.com -P @appl3 
#
# - Running without supplying parameters, it will request them individually
# sh submitIOSBuild.sh

##### Constants
JENKINSMOBILEAPPURL="http://10.100.6.51:8080/job/mobile-app-prod-ios"

#####-------- Main ---------------------------
while getopts u:p:b:U:P: option
do
  case "${option}"
  in
  u) JENKINSUSERNAME=${OPTARG};;
  p) JENKINSPASSWORD=${OPTARG};;
  b) JENKINSBUILDNUM=${OPTARG};;
  U) APPLEUSERNAME=${OPTARG};;
  P) APPLEPASSWORD=${OPTARG};;
  esac
done

#### Request variables if they're not provided
if [[ -z $JENKINSUSERNAME ]]; then
  read -p "Jenkins Username: " JENKINSUSERNAME
fi
if [[ -z $JENKINSPASSWORD ]]; then
  read -p "Jenkins Password: " JENKINSPASSWORD
fi
if [[ -z $JENKINSBUILDNUM ]]; then
  read -p "Jenkins Build#: " JENKINSBUILDNUM
fi
if [[ -z $APPLEUSERNAME ]]; then
  read -p "Apple Username: " APPLEUSERNAME
fi
if [[ -z $APPLEPASSWORD ]]; then
  read -p "Apple Password: " APPLEPASSWORD
fi

#### Attempt to download the build file. AMCS.ipa
echo "--- attempting to download the build from Jenkins ---"
if [[ $JENKINSBUILDNUM -eq "latest"  ]]; then
  BUILDURLSTRING=$JENKINSMOBILEAPPURL/lastSuccessfulBuild/artifact/trunk/AMCTheatreApp/iOS/bin/iPhone/Prod/AMCTheatreAppiOS.ipa
  IPAFILENAME="AMCTheatreAppiOS.ipa"
else
  BUILDURLSTRING=$JENKINSMOBILEAPPURL/$JENKINSBUILDNUM/artifact/trunk/AMCTheatreApp/iOS/bin/iPhone/Prod/AMCTheatreAppiOS.ipa
  IPAFILENAME=AMC$JENKINSBUILDNUM.ipa
fi
echo $BUILDURLSTRING
echo "saving as: "$IPAFILENAME
curl $BUILDURLSTRING --output $IPAFILENAME --user $JENKINSUSERNAME:$JENKINSPASSWORD

#### Check if the response has an error, I'll just filesize to determine success
FILESIZE=$(stat -f%z $IPAFILENAME)
echo "filesize: "$FILESIZE
if [[ FILESIZE -lt 1000 ]]; then
  echo "===== download failed ====="
  echo "see error.log"
  mv $IPAFILENAME error.log
  cat error.log
  exit
fi
echo "--- successfully downloaded the build from Jenkins ---"

#### Attempt to validate the app (I think this may be redundant)
echo "--- Attempting to Validate the app ---"
xcrun altool --validate-app -u $APPLEUSERNAME -p $APPLEPASSWORD -f $IPAFILENAME -t ios
if [ $? -eq 0 ]; then
  echo "--- Validation Succeeded ---"
else
  echo "--- Validation Failed ---"
  exit
fi

### Attempt to upload the app
echo "--- Attempting to Upload App to Apple"
xcrun altool --upload-app -u $APPLEUSERNAME -p $APPLEPASSWORD -f $IPAFILENAME -t ios
if [ $? -eq 0 ]; then
 echo "--- Upload Succeeded ---"
else
  echo "--- Upload Failed ---"
  exit
fi

